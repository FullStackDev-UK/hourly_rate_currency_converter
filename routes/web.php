<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('home'); })->name('home');

Route::get('/user/create', 'UserController@create')->name('create-user');
Route::get('/user/search', 'UserController@search')->name('search-users');

Route::get('/user/{user_id}', 'UserController@getUserById')->name('get-user-by-id');
Route::get('/user/created', 'UserController@created')->name('user-created');

Route::post('/user/store', 'UserController@store')->name('store-user');

Route::post('/convert-currency', 'UserController@getUserRate')->name('convert-currency');

Route::get('/show-converted-rate', 'UserController@showConvertedRate')->name('show-converted-rate');

/* Route not found or inaccessible (404,500,etc) */
Route::fallback(function () { return view('fallback'); });