<?php

namespace App\Http\Controllers;

use App\User;
use Exception;
use Illuminate\Http\Request;
use App\CurrencyConverter\Converter;
use App\CurrencyConverter\CurrencyConverter;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\CurrencyConverter\CurrencyConverterManager as CurrencyConverterManager;
use App\CurrencyConverter\Drivers\Local;
use Illuminate\Contracts\Foundation\Application;

class UserController extends Controller
{
    /**
     * Display form to create a new user
     */

    public function create() {
        return view('create-user');
    }

    /**
     * Store a new user
     * 
     * @param Request $request
     * @return Response
     */

    public function store(Request $request) {

        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'hourly_rate' => 'required',
            'currency' => 'required',
        ]);

        if ( !$validated ) {
            return redirect('/user/create')
                ->withErrors($validated)
                ->withInput();
        }

        $user = new User($request->all());
        $user->save();
        
        return view('created-user')->with('user', $user);

        return Response::make('User created', 200);
    }

    /**
     * Display form to retrieve a user
     */

    public function search() {
        $users = User::all();
        return view('search-users')->with('users', $users);
    }

    /**
     * Display a user's hourly rate in the requested currency
     */
    public function getUserById($id) {
        $user = User::where('id', $id)->first();
        return view('show-user')->with('user', $user);
    }

    /**
     * Display a user's hourly rate in the requested currency
     */
    public function getUserRate(Request $request, CurrencyConverterManager $converter) {
        try {
            $user = User::where('id', $request->user_id)->first();
            $currency = $user->currency;
            $users_currency_symbol = config('app.currencysymbols.'.$currency) ?? '';
            $hourly_rate = $user->hourly_rate;

            $requested_currency = $request->requested_currency;

            $local_rates = $converter->convert($hourly_rate, $currency, $requested_currency);
            $external_rates = $converter->driver('external')->convert($hourly_rate, $currency, $requested_currency);

            $requested_currency_symbol = config('app.currencysymbols.'.$requested_currency) ?? '';
            return view('show-converted-rate')
                ->with('user', $user)
                ->with('hourly_rate', $hourly_rate)
                ->with('currency', $currency)
                ->with('requested_currency', $requested_currency)
                ->with('local_rates', $local_rates)
                ->with('external_rates', $external_rates)
                ->with('users_currency_symbol', $users_currency_symbol)
                ->with('requested_currency_symbol', $requested_currency_symbol);


        } catch (Exception $e) {

            redirect('/user/search')->with('errors', $e);

        }

    }

    public function showConvertedRate(Request $request) 
    {
        return view('show-converted-rate');
    }
}
