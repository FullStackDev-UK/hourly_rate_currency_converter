<?php

namespace App\CurrencyConverter;

use Config;
use GuzzleHttp\Client;
use Illuminate\Support\Manager;
use App\CurrencyConverter\CurrencyConverter;
use App\CurrencyConverter\Drivers\Local;
use App\CurrencyConverter\Drivers\DriverInterface;
use App\CurrencyConverter\Drivers\External;

class CurrencyConverterManager extends Manager
{
    /**
     * Get the default currency converter driver
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return 'local';
    }

    /**
     * Get the local currency converter driver
     *
     * @return string
     */
    public function createLocalDriver()
    {
        return app(Local::class);
    }

    /**
     * Call the external currency converter driver
     *
     * @return string
     */
    public function createExternalDriver()
    {
        return app(External::class);
    }
}