<?php

namespace App\CurrencyConverter\Drivers;

use DateTime;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use App\CurrencyConverter\Drivers\DriverInterface;

class External implements DriverInterface {

    protected $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function convert(int $hourly_rate, string $from, string $to): float
    {
        $api_key = 'YOUR_exchangeratesapi.io_API_KEY';

        $client = new Client();

        $rates = Cache::get('exchange_rates_api_cache');

        if (! isset($rates) ) {
            // Call the ExchangeRatesApi.io API and cache the response for an hour
            $response = $client->request('GET', 'http://api.exchangeratesapi.io/v1/latest?access_key='.$api_key.'&symbols=USD,GBP,EUR&format=1');
            $rates = $response->getBody()->getContents();
            Cache::put("exchange_rates_api_cache", $rates, 3600);
            $rates = Cache::get('exchange_rates_api_cache');
        }

        /* For testing, this is a mock response (in nowdoc format)
        $sample_return = <<<'APISAMPLE'
        {
            "success":true,
            "timestamp":1625603043,
            "base":"EUR",
            "date":"2021-07-06",
            "rates":{
                "USD":1.18215,
                "GBP":0.856562,
                "EUR":1
            }
        }
        APISAMPLE;
        dump($sample_return);
        */

        $api_data = json_decode( $rates );
        $base_currency = $api_data->base; // Set, immutably, to 'EUR' on API
        $user_currency = $from; // E.g. 'GBP'
        $requested_currency = $to; // E.g. 'USD'
        
        $EUR_rate = $api_data->rates->EUR; // Set, immutably, to 1 on the API as its 'base'
        $GBP_rate = $api_data->rates->GBP; // 1 EUR = 0.86 GBP
        $USD_rate = $api_data->rates->USD; // 1 EUR = 1.18 USD

        $EURGBP_rate = $EUR_rate * $GBP_rate; // 1 EUR = 0.86 GBP
        $EURUSD_rate = $EUR_rate * $USD_rate; // 1 EUR = 1.18 USD

        $GBPEUR_rate = $GBP_rate * $EUR_rate; // 1 GBP = 1.17 EUR
        $USDEUR_rate = $USD_rate * $EUR_rate; // 1 USD = 0.86 EUR

        // EUR is coming back from the API, immutably, as 1...so GBP and USD can't be converted to EUR as they are just being multiplied by 1
        $GBPUSD_rate = ( $GBP_rate * $EUR_rate ) * $USD_rate; // 1 GBP = 1.38 USD
        $USDGBP_rate = ( $USD_rate * $EUR_rate ) * $GBP_rate; // 1 USD = 0.72 GBP

        if ( $user_currency == 'EUR' && $requested_currency == 'GBP' ) { $converted_hourly_rate = round( ( $hourly_rate * $EURGBP_rate ), 2 ); }
        if ( $user_currency == 'EUR' && $requested_currency == 'USD' ) { $converted_hourly_rate = round( ( $hourly_rate * $EURUSD_rate ), 2 ); }
        if ( $user_currency == 'GBP' && $requested_currency == 'EUR' ) { $converted_hourly_rate = round( ( $hourly_rate * $GBPEUR_rate ), 2 ); }
        if ( $user_currency == 'USD' && $requested_currency == 'EUR' ) { $converted_hourly_rate = round( ( $hourly_rate * $USDEUR_rate ), 2 ); }
        if ( $user_currency == 'GBP' && $requested_currency == 'USD' ) { $converted_hourly_rate = round( ( $hourly_rate * $GBPUSD_rate ), 2 ); }
        if ( $user_currency == 'USD' && $requested_currency == 'GBP' ) { $converted_hourly_rate = round( ( $hourly_rate * $USDGBP_rate ), 2 ); }

        return $converted_hourly_rate ?? 0000;
    }
}