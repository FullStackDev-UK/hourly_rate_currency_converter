<?php

namespace App\CurrencyConverter\Drivers;

interface DriverInterface
{
    public function convert(int $hourly_rate, string $from, string $to): float;
}

