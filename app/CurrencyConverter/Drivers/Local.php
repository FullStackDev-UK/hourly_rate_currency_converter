<?php

namespace App\CurrencyConverter\Drivers;

use Illuminate\Support\Facades\Config;
use App\CurrencyConverter\Drivers\DriverInterface;

class Local implements DriverInterface {

    protected $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function convert(int $hourly_rate, string $from, string $to): float
    {
        return round( $hourly_rate * config('app.localrates.'.$from.$to), 2 ) ?? 0000;
    }
}