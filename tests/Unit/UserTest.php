<?php

namespace Tests\Unit;

use App\User;
use database\factories\UserFactory;
use Tests\TestCase;
use GuzzleHttp\Client;
use Illuminate\Foundation\Testing\RefreshDatabase;

 // I'm in the process of updating these tests to Laravel 8 as Laravel 8's factory changes broke them
class UserTest extends TestCase
{
    // use RefreshDatabase;

    /**
     * A test to create a user directly
     *
     * @return void
     */
    public function testItCanCreateUserDirectlyTest()
    {
        $user = new User([
            'name' => 'Testabelle McTestingdon',
            'email' => 'test'.rand(1, 9999).'@'.rand(1, 9999).'testable.com',
            'hourly_rate' => 29,
            'currency' => 'USD'
        ]);
        $user->save();
        $retrieved_user = User::where('id', $user->id)->first(); // $user->id in this context is the ID of the newly-created User, above, and first() returns an array

        //$user2 = factory(User::class)->create(); // Create a 2nd User using a factory the old Laravel 7 way. Laravel 8 has a breaking change for factories
        $user2 = User::factory()->count(1)->create();
        $retrieved_user2 = User::where('id', $user2->id)->first();

        dump($user->name, $retrieved_user['name'], $user2->name, $retrieved_user2['name']);

        $this->assertTrue(true, $user->name === $retrieved_user['name']);
        $this->assertTrue(true, $user2->name === $retrieved_user2['name']);
    }

    /**
     * A test to create a user via posting to a route
     *
     * @return void
     */
    public function testItCanCreateUserByPostingToRouteTest()
    {
        $user = User::factory()->create(); // Old method, from Laravel 7: factory(User::class)->create();
        $retrieved_user = User::where('id', $user->id)->first();

        $base_uri = config('app.url'); // localhost
        $app_path = '../../../hourly_rate_currency_converter/public/';
        $user_path = 'user/' . $retrieved_user->id;
        $needle = false;

        $client = new Client(['base_uri' => $base_uri]);
        $response = $client->request('GET', $app_path.$user_path);

        $response_stream = fopen('php://temp', 'r+');
        fputs($response_stream, $response->getBody());
        rewind($response_stream);
        $user_from_call = stream_get_contents($response_stream);
        fclose($response_stream);

        // dump($base_uri, $app_path, $user_path, $user->name, $retrieved_user['name'], $response, $user_from_call);

        if ( strpos( $user_from_call, $user->name ) !== false ) {
            $needle = true; // dump("Needle found in haystack");
        }
        
        $this->assertTrue(true, $user->name === $retrieved_user['name']);
        $this->assertTrue(true, $needle === true);
    }

}
