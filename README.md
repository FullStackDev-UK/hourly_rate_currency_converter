# hourly_rate_currency_converter

Hourly rate currency converter

1. Change directory into your server root and clone the repo into a folder:
	> git clone git@gitlab.com:FullStackDev-UK/hourly_rate_currency_converter.git subfolder_name


2. Allow writing to storage and bootstrap folders
	> chmod 777 ./storage/ ./bootstrap/ -R


3. Install the project's dependencies by running the following at the project's root
	> composer install && npm install && npm run dev


4. Generate an application key
	> php artisan key:generate


5. Create a database on your server called hrccdb using utf8mb4_unicode_ci


6. Replace the YOUR_DATABASE, YOUR_USERNAME and YOUR_PASSWORD placeholders in the .env file with your real credentials


7. Run migrations
	> php artisan migrate


8. Go to *your_server_root*/subfolder_name/hourly_rate_currency_converter/public


9. Use the form on the first page to add users


10. Get an API key for http://api.exchangeratesapi.io


11. Replace the 'YOUR_exchangeratesapi.io_API_KEY' in app/CurrencyConverter/Drivers/External.php (between the single quotes) with the api key you get when you sign up


12. Clicking either of the buttons, "Create a user" or "Search a user's rates" proceeds along self-explanatory paths


***Note on tests*** The included tests were written for Laravel 7 and are now being updated to work with Laravel 8's (breaking) Factory changes.