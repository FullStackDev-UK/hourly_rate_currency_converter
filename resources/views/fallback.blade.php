@extends('layouts.app')

@section('content')

    <div class="content">

        <h3>Sorry, but that route doesn't exist</h3>

        <p><a href="{{ route('home') }}">Back</a></p>
        
    </div>

@endsection
