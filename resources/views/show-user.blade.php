@extends('layouts.app')

@section('content')

    <div class="content">

        <h5>Currency converted from {{$user->currency ?? 'none, as the User is unset'}} to {{ $requested_currency ?? 'nothing as, again, no User was selected'}}</h5>

        @if ( $user )
            <table class="table">
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user['email'] }}</td>
                    <td>{{ $user['hourly_rate'] }}</td>
                    <td>{{ $user->currency }}</td>
                </tr>
            </table>
        @else

            <p class="mx-auto" style="width: auto; text-align: center;">No user selected</p>

        @endif

        <p class="mx-auto" style="width: auto; text-align: center;"><a class="btn btn-primary button rounded shadow" href="{{ route('search-users') }}">Back</a></p>
        
    </div>

@endsection
