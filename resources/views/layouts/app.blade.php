<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    </head>
    <body>
        <div class="container vh-100 p-3">

            <div class="jumbotron m-0 margin-none shadow rounded gradient-down">
                <div class="title text-white text-shadow">
                    <h1 class="mx-auto" style="width: auto; text-align: center;">Hourly rate currency converter</h1>
                </div>
            </div>        

            <div class="flex-center position-ref mt-5">

                <main>
                    @yield('content')
                </main>

            </div>
            
        </div>
        <!-- Scripts -->
        <script src="{{ asset('/js/app.js') }}" defer></script>
    </body>
</html>
