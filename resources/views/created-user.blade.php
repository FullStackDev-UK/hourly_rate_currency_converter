@extends('layouts.app')

@section('content')

    <div class="content">

        <p class="mx-auto" style="width: auto; text-align: center;">The user has been created</p>
        
        <p class="mx-auto" style="width: auto; text-align: center;"><a class="btn btn-primary button rounded shadow" href="{{ route('home') }}">Back</a></p>
    </div>

@endsection