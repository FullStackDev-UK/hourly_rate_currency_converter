@extends('layouts.app')

@section('content')

    <div class="content">

        @if ($users)

            <div class="form mx-auto" style="width:600px;">
                <form name="user-form" method="post" action="{{ route('convert-currency') }}">

                    <div class="formRow mb-3">
                        <label for="user_id">Select a user to see their rates:</label>
                        <select name="user_id" id="user_id" required>
                            <option>Select</option>
                            @foreach ($users->all() as $user)
                                <option value="{{ $user->id }}">{{ $user->name }} - ({{ $user->hourly_rate }} {{ $user->currency }}/hr.)</option>
                            @endforeach
                        </select>
                    </div>
                    
                    <div class="formRow mb-3">
                        <label for="requested_currency">What currency would you like to see their rates in:</label>
                        <select name="requested_currency" id="requested_currency" required>
                            <option>Select</option>
                            <option value="GBP">GBP</option>
                            <option value="EUR">EUR</option>
                            <option value="USD">USD</option>
                        </select>
                    </div>

                    {!! csrf_field() !!}

                    <div class="buttons">
                        <div class="buttons mx-auto my-3" style="width:200px;">
                            <a href="{{ route('home') }}" class="btn btn-secondary button rounded shadow mr-3">Cancel</a>
                            <input type="submit" value="Convert" class="btn btn-primary button rounded shadow" />
                        </div>
                    </div>

                </form>
            </div>

        @else

            <p>There are no users yet</p>

        @endif

    </div>
@endsection