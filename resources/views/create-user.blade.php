@extends('layouts.app')

@section('content')

    <div class="content">
        
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            {{$errors}}
        @endif

        <div class="form mx-auto gradient-up p-3 rounded border shadow" style="width:440px;">
            <h5 class="mb-3">Create a user</h5>
            <form name="user-form" method="post" action="{{ route('store-user') }}">
                
                <div class="formRow mb-3">
                    <label class="label" style="width: 140px;" for="name">Name:</label>
                    <input type="text" style="width: 240px;" name="name" id="name" placeholder="Name" required />
                </div>

                <div class="formRow mb-3">
                    <label class="label" style="width: 140px;" for="email">Email:</label>
                    <input type="email" style="width: 240px;" name="email" id="email" placeholder="email@email.com" required />
                </div>
                
                <div class="formRow mb-3">
                    <label class="label" style="width: 140px;" for="hourly_rate">Hourly rate:</label>
                    <input type="number" style="width: 240px;" name="hourly_rate" id="hourly_rate" placeholder="00" required />
                </div>
                
                <div class="formRow mb-3">
                    <label class="label" style="width: 140px;" for="currency">Currency:</label>
                    <select style="width: 240px;" name="currency" id="currency" required>
                        <option>Select</option>
                        <option value="GBP">GBP</option>
                        <option value="EUR">EUR</option>
                        <option value="USD">USD</option>
                    </select>
                </div>

                {!! csrf_field() !!}

                <div class="buttons mx-auto my-3" style="width:200px;">
                    <a href="{{ route('home') }}" class="btn btn-secondary button rounded shadow mr-3">Cancel</a>
                    <input type="submit" value="Save user" class="btn btn-primary button rounded shadow" />
                </div>

            </form>
        </div>
    </div>

@endsection