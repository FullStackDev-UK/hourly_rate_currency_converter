@extends('layouts.app')

@section('content')

    <div class="content">

            <div class="buttons mx-auto flex" style="width: 360px;">
                <a href="{{ route('create-user') }}" class="btn btn-primary button rounded shadow mr-3">Create a user</a>
                <a href="{{ route('search-users') }}" class="btn btn-primary button rounded shadow">Search a user's rates</a>
            </div>

    </div>

@endsection
