@extends('layouts.app')

@section('content')

    <div class="content">

        <h3 class="mx-auto m-3" style="width: auto; text-align: center;">You asked about {{ $user->name ?? 'an unspecified user' }}...</h3>

        @if ( $user )

            <table class="table">
                <tr>
                    <th>Name</th>
                    <th>Email address</th>
                    <th>Hourly rate</th>
                </tr>
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user['email'] }}</td>
                    <td>{{ $users_currency_symbol }}{{ $user['hourly_rate'] }} ({{ $user->currency }})</td>
                </tr>
            </table>

            <h3 class="mx-auto m-3" style="width: auto; text-align: center;">
                <td>Here is @php echo ( in_array( $user->name, ["Mr.", "Ms.", "Mrs.", "Dr.", "Prof.", "Sir", "Lord"] ) ) ? strtok(' ') : strtok( $user->name, ' ' ); @endphp's hourly rate converted to your chosen currency<br /><div class='text-muted sub-text mt-1'>(through both local and external drivers)</div></td>
            </h3>

            <table class="table alert-info">
                <tr>
                    <th>Hourly rate</th>
                    <th>Original currency</th>
                    <th>Requested currency</th>
                    <th>At local rates</th>
                    <th>At external rates</th>
                </tr>
                <tr>
                    <td><strong><b><span>{{ $users_currency_symbol }}{{ $hourly_rate ?? 'Unset' }}</span></b></strong></td>
                    <td>{{ $user->currency ?? 'Unset' }}</td>
                    <td>{{ $requested_currency ?? 'Unset' }}</td>
                    <td><strong><b><span>{{ $requested_currency_symbol }}{{ $local_rates ?? 'Unset' }}</span></b></strong></td>
                    <td><strong><b><span>{{ $requested_currency_symbol }}{{ $external_rates ?? 'Unset' }}</span>*</b></strong></td>
                </tr>
            </table>

            <h3>*Full details:</h3>
            <p>
                <a data-toggle="collapse" href="#showFullDetails" role="button" aria-expanded="false" aria-controls="showFullDetails">+ Click to expand full details:</a>
            </p>

            <div class="row mb-3">
                <div class="col">
                    <div class="collapse" id="showFullDetails">
                        <div class="card card-body">
                            <p>
                                The free subscription plan for exchangeratesapi.io doesn't allow conversion between currencies, instead returning either the latest or historical rates (see <strong><b><a data-toggle="collapse" href="#callAndResponseOne" role="button" aria-expanded="false" aria-controls="callAndResponseOne">'Call and response #1'</a></b></strong> below), so after a call to retrieve the latest conversion rates the conversion is done locally using the retrieved values.<br /><br />

                                In light of the fact that user X can store their hourly rate in USD, and user Y can request user X's hourly rate in GBP, coupled with the fact that the base rate of exchangeratesapi.io is restricted to 'EUR = 1' and can't be changed (see <strong><b><a data-toggle="collapse" href="#callAndResponseTwo" role="button" aria-expanded="false" aria-controls="callAndResponseTwo">Call and response #2</a></b></strong> below), if user X's currency (the 'original' currency for simplicity's sake, as oppossed to the 'requested' currency) is not EUR, it must first be converted to Euro (which, because of the aforementioned base-rate restriction, is always only returned as '1' - a sample response is included in the comments in the External class) and then converted back into the requested currency. For example: (User's currency) GBP -> (Base rate) EUR -> (Requested currency) USD.
                            </p>

                            <p>
                                The API response <strong><b>is cached for one hour</b></strong>, so any calls to the API after the first one only hit the cache. After an hour the cache will have expired and the next time the rates are required a new call will be made to the API to get fresh data.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row mb-5">
                <div class="col mb-3">
                    <div class="collapse" id="callAndResponseOne">
                        <div class="card card-body">
                            <h5>Call and response #1:</h5>
                                <pre>
                                    <strong><b>Call: http://api.exchangeratesapi.io/v1/<span>convert</span>?access_key=XXXXXXX&from='.$from.'&to='.$to.'&amount='.$hourly_rate</b></strong>
                                    <strong><b>Response: </b></strong> {
                                        "error": {
                                            "code": "<strong><b>Function_access_restricted</b></strong>",
                                            "message": "Access Restricted - Your current Subscription Plan does not support this API Function."
                                        }
                                    }
                                </pre>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="collapse" id="callAndResponseTwo">
                        <div class="card card-body">
                            <h5>Call and response #2:</h5>
                                <pre>
                                    <strong><b>Call: http://api.exchangeratesapi.io/v1/convert?access_key=XXXXXXX<span>&base='.$from.'</span>&from='.$from.'&to='.$to.'&amount='.$hourly_rate</b></strong>
                                    <strong><b>Response: </b></strong> {
                                        "error": {
                                            "code": "<strong><b>base_currency_access_restricted</b></strong>",
                                            "message": "An unexpected error ocurred. [Technical Support: support@apilayer.com]"
                                        }
                                    }
                                </pre>
                        </div>
                    </div>
                </div>
            </div>

            <p class="mx-auto mt-3" style="width: auto; text-align: center;">
                <a class="btn btn-warning rounded shadow mr-3" data-toggle="collapse" href="#callAndResponseOne" role="button" aria-expanded="false" aria-controls="callAndResponseOne">Show/hide call and response #1</a>
                <a class="btn btn-warning rounded shadow mr-3" data-toggle="collapse" href="#callAndResponseTwo" role="button" aria-expanded="false" aria-controls="callAndResponseTwo">Show/hide call and response #2</a>
                <a class="btn btn-primary rounded shadow mr-3" href="{{ route('search-users') }}">Back to users</a>
                <a class="btn btn-secondary rounded shadow" href="{{ route('home') }}">Home</a>
            </p>
        @else

            <p>No user selected</p>

        @endif
        
    </div>

@endsection
